package fpat.fp.armory.creativetab;

import fpat.fp.armory.research.icons.FPATArmorResearchIcons;
import futurepack.client.creative.TabFB_Base;
import futurepack.common.block.FPBlocks;

import net.minecraft.item.ItemStack;

public class FPATArmorTab extends TabFB_Base {

	public FPATArmorTab(int id, String label) 
	{
		super(id, label);
	}

	
	@Override
	public ItemStack getTabIconItem()
	{
		return new ItemStack(FPATArmorResearchIcons.research_tab_pages,1,0);
	}
}
