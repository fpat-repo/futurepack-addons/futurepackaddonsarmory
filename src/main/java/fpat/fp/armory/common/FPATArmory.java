package fpat.fp.armory.common;

import java.io.File;

import fpat.fp.armory.creativetab.FPATArmorTab;
import futurepack.client.creative.TabFB_Base;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;


@Mod(modid = FPATArmory.modID, name = FPATArmory.modName, version = FPATArmory.modVersion, dependencies = "required-after:fp;" + "required-after:fpatcore")
public class FPATArmory
{
	public static final String modID = "fpatarmory";
	public static final String modName = "Futurepack Addon - Armory";
	public static final String modVersion = "Version.version";
	
	@Instance(FPATArmory.modID)
	public static FPATArmory instance;
	
	public FPATArmory()
	{
		
	}

	public static TabFB_Base FPTab = new FPATArmorTab(CreativeTabs.getNextID(), "FPATArmorTab");


	@SidedProxy(modId=FPATArmory.modID, clientSide="fpat.fp.armory.common.FPATArmoryProxyClient", serverSide="fpat.fp.armory.common.FPATArmoryProxyServer")
	public static FPATArmoryProxyBase proxy;
	
	
	@EventHandler
    public void preInit(FMLPreInitializationEvent event) 
    {	
    	proxy.preInit(event);
    }
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		proxy.load(event);
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		proxy.postInit(event);
	}
	
	@EventHandler
	public void preServerStart(FMLServerAboutToStartEvent event) 
	{
		
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) 
	{		

	}
	
	@EventHandler
	public void serverStarted(FMLServerStartedEvent event) 
	{

	}
	
	
	@EventHandler
	public void serverStopped(FMLServerStoppingEvent event) 
	{

	}
}