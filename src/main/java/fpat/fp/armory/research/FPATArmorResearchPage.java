package fpat.fp.armory.research;

import fpat.fp.armory.common.FPATArmory;
import fpat.fp.armory.research.icons.FPATArmorResearchIcons;
import futurepack.api.event.ResearchPageRegisterEvent;
import futurepack.common.item.FPItems;
import futurepack.common.research.ResearchLoader;
import futurepack.common.research.ResearchPage;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = FPATArmory.modID)
public class FPATArmorResearchPage
{

	public static ResearchPage fpatarmor;
	
	/**
	 * Used in {@link ResearchLoader} to make sure, <i>/fp research reload</i>, works.
	 */
	
	@SubscribeEvent
    public static void init(ResearchPageRegisterEvent event)
	{
		fpatarmor = new ResearchPage("fpatarmor").setIcon(new ItemStack(FPATArmorResearchIcons.research_tab_pages, 1, 0));
	};
	
}
