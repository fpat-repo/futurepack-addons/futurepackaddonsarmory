package fpat.fp.armory.research;

import java.io.InputStreamReader;

import futurepack.common.FPLog;
import futurepack.common.research.ResearchLoader;

public class FPATArmorResearchLoader
{
	
	public static void init()
	{
		try
		{
			ResearchLoader.instance.addResearchesFromReader(new InputStreamReader(FPATArmorResearchLoader.class.getResourceAsStream("research.json")));
		}
		catch(Exception ex)
		{
			FPLog.logger.error("Failed to load research.json for FPATArmory!");
			ex.printStackTrace();
		}
	}
}
