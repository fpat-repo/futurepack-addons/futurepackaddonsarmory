package fpat.fp.armory.research.icons;

import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemsIcons extends Item implements IItemMetaSubtypes {
	
	String[] names = new String[]{"armory_matrix"};
	
	public static final int color_matrix = 0;
	public ItemsIcons()
	{
		super();
		this.setHasSubtypes(true);
	}
	
	@Override
	public String getUnlocalizedName(ItemStack is) 
	{
		if(is.getItemDamage()<names.length)
			return super.getUnlocalizedName(is) + "_" + names[is.getItemDamage()];
		
		return super.getUnlocalizedName(is);
	}

	@Override
	public int getMaxMetas() 
	{
		return names.length;
	}

	@Override
	public String getMetaName(int meta) 
	{
		return names[meta];
	}
}
