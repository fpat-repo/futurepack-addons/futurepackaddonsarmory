package fpat.fp.armory.research.icons;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import fpat.fp.armory.common.FPATArmory;
import fpat.fp.armory.research.icons.ItemsIcons;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;



public class FPATArmorResearchIcons {
	
	public static List<Item> itemBlocks = new ArrayList<Item>();
	
	//research tabs
	public static final Item research_tab_pages = new ItemsIcons().setUnlocalizedName("icon");
	
	public static void register(RegistryEvent.Register<Item> reg)
	{
		
		IForgeRegistry<Item> r = reg.getRegistry();

		//research tabs
		registerItem(research_tab_pages, "icon", r);
	}

	private static void registerItem(Item item, String string, IForgeRegistry<Item> reg) {

		ResourceLocation res = new ResourceLocation(FPATArmory.modID, string);
		item.setRegistryName(res);
		reg.register(item);
	}
	

	@SideOnly(Side.CLIENT)
	public static void setupRendering()
	{
		
		try
		{
			Field[] fields = FPATArmorResearchIcons.class.getFields();
			for(Field f : fields)
			{
				if(Modifier.isStatic(f.getModifiers()))
				{
					if(Item.class.isAssignableFrom(f.getType()))
					{	
						Item item = (Item) f.get(null);
						
						if(item==null)
							continue;
						
						if(item instanceof IItemMetaSubtypes)
						{
							IItemMetaSubtypes meta = (IItemMetaSubtypes) item;
							for(int i=0;i<meta.getMaxMetas();i++)
							{
								registerItem(meta.getMetaName(i), item, i);
							}
						}
						else
						{
							registerItem(item.getUnlocalizedName().substring(5), item, 0);
						}
					}
 				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SideOnly(Side.CLIENT)
	private static void registerItem(String string, Item item, int meta)
	{
		RenderItem render = Minecraft.getMinecraft().getRenderItem();
		string = FPATArmory.modID +":items/" + toLoverCase(string);
		ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(string, "inventory"));
	}
	
	private static String toLoverCase(String string)
	{
		for(int i='A';i<='Z';i++)
		{
			int index = string.indexOf(i);
			if(index > 0)
			{
				char c = string.charAt(index -1);
				String s2 = c >= 'a' && c <= 'z' ? "_" : "";
				string = string.replace( "" + (char)i, (s2 +(char)i).toLowerCase() );
			}
			else
			{
				string = string.replace( "" + (char)i, ("" +(char)i).toLowerCase() );
			}
				
		}
		return string;
	}

}