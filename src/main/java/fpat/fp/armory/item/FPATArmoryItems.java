package fpat.fp.armory.item;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import javax.annotation.Nullable;

import fpat.fp.armory.common.FPATArmory;
import futurepack.common.FPMain;
import futurepack.common.item.FPItems;
import futurepack.common.item.ItemErze;
import futurepack.common.item.ItemSpaceship;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.depend.api.interfaces.IItemMetaSubtypes;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.RenderItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;
import futurepack.common.item.FPItems;

public class FPATArmoryItems
{    

	public static Item tools = new ItemSpaceship().setUnlocalizedName("spaceship");//.setTextureName("spaceship"); 

    public static ToolMaterial TMCOMPOSITE = EnumHelper.addToolMaterial("COMPOSITE", 3, 3000, 7.0f, 2.0f, 15).setRepairItem(new ItemStack(tools, 1, ItemSpaceship.CompositeMetall));
    public static ToolMaterial TMNEON = EnumHelper.addToolMaterial("NEON", 3, 3000, 25.0f, 5.0f, 15).setRepairItem(new ItemStack(tools, 1, ItemSpaceship.CompositeMetall));
    
    public static Item composite_shears = new ItemCompositeShears().setUnlocalizedName("composite_shears");
    public static Item neon_shears = new ItemNeonShears().setUnlocalizedName("neon_shears");
    public static Item composite_sword = new ItemCompositeSword(TMCOMPOSITE).setUnlocalizedName("composite_sword");
    public static Item neon_hoe = new ItemNeonHoe(TMNEON).setUnlocalizedName("neon_hoe");
    public static Item neon_pickaxe = new ItemNeonPickaxe(TMNEON).setUnlocalizedName("neon_pickaxe");
    public static Item neon_shovel = new ItemNeonShovel(TMNEON).setUnlocalizedName("neon_shovel");
    public static Item neon_axe = new ItemNeonShovel(TMNEON).setUnlocalizedName("neon_axe");
	
	public static void registry(RegistryEvent.Register<Item> reg)
	{
		IForgeRegistry<Item> r = reg.getRegistry();
		
		registerItem(composite_shears, "composite_shears", r);
		registerItem(neon_shears, "neon_shears", r);
		registerItem(composite_sword, "composite_sword", r);
		registerItem(neon_hoe, "neon_hoe", r);
		registerItem(neon_pickaxe, "neon_pickaxe", r);
		registerItem(neon_shovel, "neon_shovel", r);
		registerItem(neon_axe, "neon_axe", r);
	}
	private static void registerItem(Item item, String string, IForgeRegistry<Item> reg) {

		ResourceLocation res = new ResourceLocation(FPATArmory.modID, string);
		item.setRegistryName(res);
		reg.register(item);
	}
	

	@SideOnly(Side.CLIENT)
	public static void setupRendering()
	{
		
		try
		{
			Field[] fields = FPATArmoryItems.class.getFields();
			for(Field f : fields)
			{
				if(Modifier.isStatic(f.getModifiers()))
				{
					if(Item.class.isAssignableFrom(f.getType()))
					{	
						Item item = (Item) f.get(null);
						
						if(item==null)
							continue;
						
						if(item instanceof IItemMetaSubtypes)
						{
							IItemMetaSubtypes meta = (IItemMetaSubtypes) item;
							for(int i=0;i<meta.getMaxMetas();i++)
							{
								registerItem(meta.getMetaName(i), item, i);
							}
						}
						else
						{
							registerItem(item.getUnlocalizedName().substring(5), item, 0);
						}
					}
 				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@SideOnly(Side.CLIENT)
	private static void registerItem(String string, Item item, int meta)
	{
		RenderItem render = Minecraft.getMinecraft().getRenderItem();
		string = FPATArmory.modID +":items/" + toLoverCase(string);
		ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(string, "inventory"));
	}
	
	private static String toLoverCase(String string)
	{
		for(int i='A';i<='Z';i++)
		{
			int index = string.indexOf(i);
			if(index > 0)
			{
				char c = string.charAt(index -1);
				String s2 = c >= 'a' && c <= 'z' ? "_" : "";
				string = string.replace( "" + (char)i, (s2 +(char)i).toLowerCase() );
			}
			else
			{
				string = string.replace( "" + (char)i, ("" +(char)i).toLowerCase() );
			}
				
		}
		return string;
	}
}
