package fpat.fp.armory.item;

import fpat.fp.armory.common.FPATArmory;
import net.minecraft.item.ItemShears;
import net.minecraft.item.ItemStack;

public class ItemCompositeShears extends ItemShears
{

    public ItemCompositeShears()
    {
        this.setMaxStackSize(1);
        this.setMaxDamage(3000);
        this.setCreativeTab(FPATArmory.FPTab);
    }
}
