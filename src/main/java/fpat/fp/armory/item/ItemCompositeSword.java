package fpat.fp.armory.item;

import fpat.fp.armory.common.FPATArmory;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSword;

public class ItemCompositeSword extends ItemSword
{

    public ItemCompositeSword(ToolMaterial material) {
		super(material);
        this.maxStackSize = 1;
        this.setMaxDamage(3000);
        this.setCreativeTab(FPATArmory.FPTab);
	}
}
