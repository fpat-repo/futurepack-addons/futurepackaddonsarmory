package fpat.fp.armory.item;

import java.util.List;

import fpat.fp.armory.common.FPATArmory;
import futurepack.api.interfaces.IItemNeon;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

public class ItemNeonPickaxe extends ItemPickaxe implements IItemNeon
{

	public ItemNeonPickaxe(Item.ToolMaterial material)
    {
    	super(material);
        this.toolMaterial = material;
        this.maxStackSize = 1;
        this.setMaxDamage(0);
        this.setCreativeTab(FPATArmory.FPTab);
//        this.speed = material.getAttackDamage() + 1.0F;
    }
	
    @Override
	public boolean isNeonable(ItemStack it)
	{
		return true;
	}

	@Override
	public void addNeon(ItemStack it, int i)
	{
		int ne = getNeon(it) + i;
		if(ne<0)
			ne=0;
		
		NBTTagCompound nbt = it.getSubCompound("neon");
		if(nbt==null)
		{
			nbt = new NBTTagCompound();		
		}
		nbt.setInteger("ne", ne);
		it.setTagInfo("neon", nbt);
	}

	@Override
	public int getMaxNeon(ItemStack it) 
	{
		return 800;
	}

	@Override
	public int getNeon(ItemStack it)
	{
		NBTTagCompound nbt = it.getSubCompound("neon");
		if(nbt==null)
		{
			return 0;
		}
		return nbt.getInteger("ne");
	}
	
	@Override
	public void addInformation(ItemStack it, World w, List l, ITooltipFlag p_77624_4_) 
	{
		l.add(getNeon(it) + "/" + getMaxNeon(it));
		super.addInformation(it, w, l, p_77624_4_);
	}
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		return MathHelper.hsvToRGB(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack)
	{
		return 1- ((double)getNeon(stack) / (double)getMaxNeon(stack));
	}
	
	@Override
	public boolean showDurabilityBar(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
}
