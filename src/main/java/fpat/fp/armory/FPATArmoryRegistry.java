package fpat.fp.armory;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import fpat.fp.armory.common.FPATArmory;
import fpat.fp.armory.item.FPATArmoryItems;
import fpat.fp.armory.research.icons.FPATArmorResearchIcons;

@Mod.EventBusSubscriber(modid = FPATArmory.modID)
public class FPATArmoryRegistry {
	
	@SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
//		FPCABlocks.register(event);
	}
	
	@SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
	{
//		event.getRegistry().registerAll(FPCABlocks.itemBlocks.toArray(new Item[FPCABlocks.itemBlocks.size()]));
//		FPCABlocks.itemBlocks.clear();
//		FPCABlocks.itemBlocks = null;
		

		FPATArmoryItems.registry(event);
		FPATArmorResearchIcons.register(event);
	}
	
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
		try
		{
//			FPCABlocks.setupPreRendering();
		}
		catch(NoSuchMethodError e){}
		FPATArmoryItems.setupRendering();
		FPATArmorResearchIcons.setupRendering(); 
	}
}
